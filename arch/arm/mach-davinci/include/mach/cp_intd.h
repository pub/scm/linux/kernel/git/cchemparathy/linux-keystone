/*
 * TI Common Platform Interrupt Distributor (cp_intd) definitions
 *
 * Copyright (C) 2011 Texas Instruments
 *
 * This file is licensed under the terms of the GNU General Public License
 * version 2. This program is licensed "as is" without any warranty of any
 * kind, whether express or implied.
 */
#ifndef __ASM_HARDWARE_CP_INTD_H
#define __ASM_HARDWARE_CP_INTD_H

void __init cp_intd_init(void);

#endif  /* __ASM_HARDWARE_CP_INTD_H */
